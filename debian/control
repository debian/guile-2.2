Source: guile-2.2
Section: interpreters
Priority: optional
Maintainer: Anthony Fok <foka@debian.org>
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13),
 libtool, autoconf, automake,
 guile-2.2:native <cross>,
 libncurses-dev, libreadline-dev, libltdl-dev, libgmp-dev, texinfo, flex,
 libunistring-dev, libgc-dev (>= 1:8), libffi-dev, pkg-config
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/debian/guile-2.2
Vcs-Git: https://salsa.debian.org/debian/guile-2.2.git
Homepage: http://www.gnu.org/software/guile/

Package: guile-2.2
Section: lisp
Architecture: any
Multi-Arch: allowed
Provides: guile
Depends: guile-2.2-libs (= ${binary:Version}), ${shlibs:Depends},
 ${misc:Depends}
Suggests: guile-2.2-doc
Description: GNU extension language and Scheme interpreter
 Guile is a Scheme implementation designed for real world programming,
 providing a rich Unix interface, a module system, an interpreter, and
 many extension languages.  Guile can be used as a standard #! style
 interpreter, via #!/usr/bin/guile, or as an extension language for
 other applications via libguile.

Package: guile-2.2-dev
Section: lisp
Architecture: any
Provides: libguile-dev (= ${binary:Version})
Conflicts: guile-1.8, libguile-dev (<< 2.2.7+1-4), guile-3.0-dev (<< 3.0.1+1-2)
Depends: ${shlibs:Depends}, guile-2.2:any (= ${binary:Version}),
 guile-2.2-libs (= ${binary:Version}), libc6-dev,
 libncurses-dev, libreadline6-dev, libltdl-dev, libgmp-dev, libgc-dev,
 pkg-config, ${misc:Depends}
Description: Development files for Guile 2.2
 This package contains files needed for development using Guile 2.2.
 .
 Guile is a Scheme implementation designed for real world programming,
 providing a rich Unix interface, a module system, an interpreter, and
 many extension languages.  Guile can be used as a standard #! style
 interpreter, via #!/usr/bin/guile, or as an extension language for
 other applications via libguile.

Package: guile-2.2-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends: dpkg (>= 1.15.4) | install-info, ${misc:Depends}
Description: Documentation for Guile 2.2
 This package contains the Guile documentation, including the Guile
 Reference Manual.
 .
 Guile is a Scheme implementation designed for real world programming,
 providing a rich Unix interface, a module system, an interpreter, and
 many extension languages.  Guile can be used as a standard #! style
 interpreter, via #!/usr/bin/guile, or as an extension language for
 other applications via libguile.

Package: guile-2.2-libs
Section: lisp
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Core Guile libraries
 Guile is a Scheme implementation designed for real world programming,
 providing a rich Unix interface, a module system, an interpreter, and
 many extension languages.  Guile can be used as a standard #! style
 interpreter, via #!/usr/bin/guile, or as an extension language for
 other applications via libguile.
